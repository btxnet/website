<!DOCTYPE html>
<html lang="de">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Justin E.">
	<meta name="keywords" content="TsRadio">
	<meta name="description" content="">

	<title>TSRadio | Musikwunsch</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
  	<link rel="stylesheet" href="css/component.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/vegas.min.css">
	<link rel="stylesheet" href="css/style.css">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300' rel='stylesheet' type='text/css'>
	
</head>
<body id="top" data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

<div class="preloader">
     <div class="sk-spinner sk-spinner-pulse"></div>
</div>

  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
          <span class="icon icon-bar"></span>
        </button>
        <a href="player.php" class="navbar-brand smoothScroll">Zum Player</a>
      </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php" class="smoothScroll"><span>Home</span></a></li>
            <li><a href="apply.php" class="smoothScroll"><span>Bewerben</span></a></li>
            <li><a href="#top" class="smoothScroll"><span>Musikwunsch</span></a></li>
            <li><a href="int/login.php" class="smoothScroll"><span>Login</span></a>
          </ul>
       </div>

    </div>
  </div>

<section id="home">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">

      <div class="col-md-offset-1 col-md-10 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
        <h1 class="wow fadeInUp" data-wow-delay="0.6s">Du willst unseren Bot auf deinem TeamSpeak?</h1>
        <p class="wow fadeInUp" data-wow-delay="0.9s">Unser Radio existiert seit Anfang 2018 hat sich zur Aufgabe gemacht, jedem TeamSpeak die Möglichkeit zu geben, einen kostenlosen Musikbot zu bekommen. Wir spielen die Aktuellste Musik und Musikwünsche jeder Art in unseren Liveshows.</p>
        <p class="wow fadeInUp" data-wow-delay="0.9s">Du möchtest unseren Stream hören? Drücke oben einfach auf den Knopf! Solltest du aber Interesse an einem unseren MusikBots haben, dann melde dich bei uns per Twitter, Email oder besuche unseren TeamSpeak. Dort erhälst du auch genauere Informationen!</p>
          <a href="ts3server://tsradio.de?port=9987" class="smoothScroll btn btn-success btn-lg wow fadeInUp" data-wow-delay="1.2s">UNSER TEAMSPEAK</a>
      </div>

    </div>
  </div>
</section>

<section id="contact">  <div class="container">
    <div class="row">
       <div class="col-md-offset-1 col-md-10 col-sm-12">

        <div class="col-lg-offset-1 col-lg-10 section-title wow fadeInUp" data-wow-delay="0.4s">
          <h1>Du hast einen Musikwunsch?</h1>
          <p>Kein Problem! Schreibe uns einfach deinen Musikwunsch.</p>
        </div>

        <form action="#" method="post" class="wow fadeInUp" data-wow-delay="0.8s">
          <div class="col-md-6 col-sm-6">
            <input name="name" type="text" class="form-control" id="name" placeholder="Musiktitel">
          </div>
          <div class="col-md-6 col-sm-6">
            <input name="email" type="email" class="form-control" id="email" placeholder="Musikartist">
          </div>
          <div class="col-md-12 col-sm-12">
            <textarea name="message" rows="6" class="form-control" id="message" placeholder="Nachricht"></textarea>
          </div>
          <div class="col-md-offset-3 col-md-6 col-sm-offset-3 col-sm-6">
            <input type="submit" class="form-control" value="MUSIKWUNSCH SENDEN">
          </div>
        </form>

      </div>
      

    </div>
  </div>
</section>

<footer>
	<div class="container">
    
		<div class="row">

			<div class="col-md-12 col-sm-12">
            
                <ul class="social-icon"> 
                   <li><a href="https://www.twitter.com/TSRadioDE" class="fab fa-twitter wow fadeInUp" data-wow-delay="0.2s"></a></li>
                    <li><a href="https://www.youtube.com/channel/UCisQ74ovJso7vkY09taj5-g" class="fab fa-youtube wow fadeInUp" data-wow-delay="0.6s"></a></li>
                </ul>

                <p class="wow fadeInUp" data-wow-delay="1s"><a href="https://calypsohost.net">CalypsoHost</a> - <a href="https://nxtserv.de/">NXTServ</a></p>
                <p class="wow fadeInUp" data-wow-delay="1s"><a href="inprint.php" >Impressum</a></p>
				<p class="wow fadeInUp" data-wow-delay="1s"><a href="http://ec.europa.eu/consumers/odr">ODR</a></p>
				<p class="wow fadeInUp"  data-wow-delay="1s">Copyright &copy; 2018 TSRadio</p>                   
			</div>
			
		</div>
        
	</div>
</footer>

<a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vegas.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/toucheffects.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>