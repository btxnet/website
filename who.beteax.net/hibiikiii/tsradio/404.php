 <!DOCTYPE html>
<html lang="de">
 
 <head>
 <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Justin E.">
	<meta name="keywords" content="TsRadio">
	<meta name="description" content="">
   <link rel="stylesheet" href="css/404.css">
 </head>
 
 <div id="clouds">
            <div class="cloud x1"></div>
            <div class="cloud x1_5"></div>
            <div class="cloud x2"></div>
            <div class="cloud x3"></div>
            <div class="cloud x4"></div>
            <div class="cloud x5"></div>
        </div>
        <div class='c'>
            <div class='_404'>404</div>
            <hr>
            <div class='_1'>DIE SEITE</div>
            <div class='_2'>WURDE NICHT GEFUNDEN</div>
            <a class='btn' href='javascript:history.back()'>ZÜRÜCK</a>
			</script>
        </div>