<!DOCTYPE html>
<html lang="de">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Justin E.">
	<meta name="keywords" content="TsRadio">
	<meta name="description" content="">

	<title>TSRadio | Impressum</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
  	<link rel="stylesheet" href="css/component.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="css/owl.theme.css">
	<link rel="stylesheet" href="css/owl.carousel.css">
	<link rel="stylesheet" href="css/vegas.min.css">
	<link rel="stylesheet" href="css/style.css">

	<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300' rel='stylesheet' type='text/css'>
	
</head>
<body id="top" data-spy="scroll" data-offset="50" data-target=".navbar-collapse">

<div class="preloader">
     <div class="sk-spinner sk-spinner-pulse"></div>
</div>

  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">

      <div class="navbar-header">
        <a href="#top" class="navbar-brand smoothScroll">IMPRESSUM | TSRadio.de</a>
      </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php" class="smoothScroll"><span>Home</span></a></li>
            <li><a href="apply.php" class="smoothScroll"><span>Bewerben</span></a></li>
            <li><a href="musicwish.php" class="smoothScroll"><span>Musikwunsch</span></a></li>
            <li><a href="int/login.php" class="smoothScroll"><span>Login</span></a>
          </ul>
       </div>
        

    </div>
  </div>

<section id="contact">
  <div class="overlay"></div>
  <div class="container">
    <div class="row">

      <div class="col-md-offset-1 col-md-10 col-sm-12 wow fadeInUp" data-wow-delay="0.3s">
        <h1 class="wow fadeInUp" data-wow-delay="0.6s">Angaben gemä&szlig; §5 TMG</h1>
        <p class="wow fadeInUp" data-wow-delay="0.7s">Max Schmidtke</p>
        <p class="wow fadeInUp" data-wow-delay="0.7s">Moltkestr. 80</p>
        <p class="wow fadeInUp" data-wow-delay="0.7s">44536 Lünen</p>
        <p class="wow fadeInUp" data-wow-delay="0.7s">Telefon: +49 1590 2269249</p>  
        <p class="wow fadeInUp" data-wow-delay="0.7s">E-Mail: info@TSRadio.de</p>
        <h1 class="wow fadeInUp" data-wow-delay="0.6s">Haftung für Inhalte</h1>
        <p class="wow fadeInUp" data-wow-delay="0.7s">Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. 
Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>
          <h1 class="wow fadeInUp" data-wow-delay="0.6s">Haftung für Links</h1>
          <p class="wow fadeInUp" data-wow-delay="0.7s">Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. 
Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>
          <h1 class="wow fadeInUp" data-wow-delay="0.6s">Urheberrecht</h1>
           <p class="wow fadeInUp" data-wow-delay="0.7s">Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. 
Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>
          <a href="https://www.erecht24.de" class="smoothScroll btn btn-success btn-lg wow fadeInUp" data-wow-delay="1.0s">QUELLE: erecht24.de</a>
      </div>

    </div>
  </div>
</section>

<footer>
	<div class="container">
    
		<div class="row">

			<div class="col-md-12 col-sm-12">
            
                <ul class="social-icon"> 
                   <li><a href="https://www.twitter.com/TSRadioDE" class="fab fa-twitter wow fadeInUp" data-wow-delay="0.2s"></a></li>
                    <li><a href="https://www.youtube.com/channel/UCisQ74ovJso7vkY09taj5-g" class="fab fa-youtube wow fadeInUp" data-wow-delay="0.6s"></a></li>
                </ul>

                 <p class="wow fadeInUp" data-wow-delay="1s"><a href="https://calypsohost.net">CalypsoHost</a> - <a href="https://nxtserv.de/">NXTServ</a></p>
                <p class="wow fadeInUp" data-wow-delay="1s"><a href="inprint.php" >Impressum</a></p>
				<p class="wow fadeInUp" data-wow-delay="1s"><a href="http://ec.europa.eu/consumers/odr">ODR</a></p>
				<p class="wow fadeInUp"  data-wow-delay="1s">Copyright &copy; 2018 TSRadio</p>                   
			</div>
			
		</div>
        
	</div>
</footer>

<a href="#" class="go-top"><i class="fa fa-angle-up"></i></a>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vegas.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/toucheffects.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/custom.js"></script>

</body>
</html>