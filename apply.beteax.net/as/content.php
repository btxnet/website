<?php
include("../inc/app.php");
include("../inc/min.php");
$userip = $_SERVER['REMOTE_ADDR'];
//Close and Exit site because there is no Apply open!
//START CLOSED
if($contentapply == 0){
header("Location: ../index?apply=closed"); 
    exit;
}
//END CLOSED
?>
    <!DOCTYPE html>
    <html lang="de">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Beteax × Apply</title>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="../css/apply.css" rel="stylesheet" media="screen">
        <link href="../css/form.css" rel="stylesheet" media="screen">
        <link href="../css/util.css" rel="stylesheet" media="screen">
        <link href="../css/main.css" rel="stylesheet" media="screen">
    </head>
    <!-- Beta Version by VariusDev (Yes still in Beta) -->


    <body>

        <nav class="navbar navbar-expand-md navbar-dark fixed-top">
            <div style="padding-top: 5px; padding-left: 10px;"><a class="navbar-brand" style="font-size:2.00rem!important;" href="../apply"><i class="fas fa-paper-plane"></i> Apply</a></div>

            <div class="collapse navbar-collapse" id="nvbx">
                <ul class="navbar-nav mr-auto">
                    <li class="collapse navbar-collapse">
                        <a class="nav-link text-muted active" href="../apply"></a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <ul class="navbar-nav mr-auto">
                        <li class="collapse navbar-collapse" id="nvbx">
                            <a class="nav-link" href="../apply">Zurück</a>
                            <a class="nav-link" href="../../shop">User Panel</a>

                        </li>
                    </ul>
                </form>
            </div>
        </nav>
        <main role="main" class="wrapper">
            <div id="page-wrapper">
                <center>
                    <!-- Contact CSS from colorlib.com ~ Coding by Beteax -->
                    <h1 align="center" class="heading-apply"><span>Bewerbung als Content</span></h1>
                    <div class="container-fluid">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="white-box">
                                <span align="center">Bewerbung als Content</span><br />
                                <small>Es werden min. <?php echo($content); ?> Wörter benötigt.</small>
                                <hr />
                                <form class="contact100-form validate-form" method="post" action="../requests/send.php">

                                    <div class="wrap-input100 rs1-wrap-input100 validate-input">
                                        <span class="label-input100">Dein Name</span>
                                        <input class="input100" type="text" name="user" placeholder="Dein Name" required>
                                        <span class="focus-input100"></span>
                                    </div>

                                    <div class="wrap-input100 rs1-wrap-input100 validate-input">
                                        <span class="label-input100">Email Adresse</span>
                                        <input class="input100" type="email" name="email" placeholder="Deine E-Mail Adresse" required>
                                        <span class="focus-input100"></span>
                                    </div>

                                    <div class="wrap-input100 rs1-wrap-input100 validate-input">
                                        <span class="label-input100">Ingamename</span>
                                        <input class="input100" type="text" name="userig" placeholder="Dein Minecraftname" required>
                                        <span class="focus-input100"></span>
                                    </div>

                                    <div class="wrap-input100 rs1-wrap-input100 validate-input">
                                        <span class="label-input100">Als</span>
                                        <input class="input100" type="text" name="as" placeholder="Support" value="Content" readonly required>
                                        <span class="focus-input100"></span>
                                    </div>

                                    <div class="wrap-input100 validate-input">
                                        <span class="label-input100">Bewerbungstext</span>
                                        <textarea class="input100" name="applytext" placeholder="Schreibe hier deinen Bewerbungstext" required></textarea>
                                        <span class="focus-input100"></span>
                                    </div>
                                    <?php 
                                    echo('<input type="hidden" value="'.$userip.'" readonly required name="userip">');
                                        ?>

                                    <div class="container-contact100-form-btn">
                                        <button class="contact100-form-btn">
                                            <span>Abschicken <i class="fas fa-arrow-right" aria-hidden="true"></i>
                                        </span>
                                    </button>
                                    </div>
                                </form>
                                <center>
                                    <p style="margin-bottom: 0!important;" class="ipuses">Zu unserer und deiner Sicherheit wird deine IP-Adresse (
                                        <?php echo($userip); ?>) mitgesendet.</p>
                                </center>
                            </div>
                        </div>
                    </div>

                </center>
            </div>
        </main>
        <script src="js/main.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <center>
            <p style="margin-bottom: 0!important;" class="copyright">&copy; 2018 Beteax Network<br /><a href="https://imprint.beteax.net">Impressum</a></p>
        </center>


    </body>

    </html>
