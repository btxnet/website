<?php if(isset($_GET['as'])){	if($_GET['as'] == 'support'){header("Location: as/support");}else if($_GET['as'] == 'moderator'){header("Location: as/moderator");}else if($_GET['as'] == 'builder'){header("Location: as/builder");}else if($_GET['as'] == 'content'){header("Location: as/content");}else if($_GET['as'] == 'developer'){header("Location: as/developer");}}?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Beteax.net - Apply Seite">
    <meta name="author" content="VariusDev">
    <link rel="icon" href="../favicon.ico">

    <title>Beteax × Apply</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Archivo+Black" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="css/apply.css" rel="stylesheet" media="screen">
    <link href="css/form.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="css/default.css" />
    <link rel="stylesheet" type="text/css" href="css/component.css" />
</head>
<!-- Beta Version by VariusDev (Yes still in Beta) -->


<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top">
        <div style="padding-top: 5px; padding-left: 10px;"><a class="navbar-brand" style="font-size:2.00rem!important;" href="#"><i class="fas fa-paper-plane"></i> Apply</a></div>

        <div class="collapse navbar-collapse" id="nvbx">
            <ul class="navbar-nav mr-auto">
                <li class="collapse navbar-collapse">
                    <a class="nav-link text-muted active" href="#"></a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <ul class="navbar-nav mr-auto">
                    <li class="collapse navbar-collapse" id="nvbx">
                        <a class="nav-link" href="../shop">User Panel</a>

                    </li>
                </ul>
            </form>
        </div>
    </nav>
	<!-- <p class="apply-holder">Applytext<br/>BSPT</p> <p class="apply-holder">  </p> -->
    <main role="main" class="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">
                <h1 align="center" class="heading-apply"><span>Als was möchtest du dich Bewerben? </span></h1>
                <div style="padding: 10px 800px 150px 190px;" class="row apply-holder">
                    <div class="white-box">
                        <?php
                        include("inc/app.php");
                        if($supportapply == 1){
                            echo '<h5 class="text-muted vb head-apply"> Bewerben als Supporter/in</h5>
                            <div class="col-in row">
                                <p class="apply-holder">
Wie man es aus dem Namen schon raus hört unterstützt der Supporter Usern bei Problemen.
<br /> Dies tut der Supporter im Forum, Teamspeak und auf dem Netzwerk.
<br/> Der Supporter Rang ist für die Personen zo empfehlen die bisher noch keine oder wenige Erfahrungen im Bereich Support und der Moderation sammlen konnten.
<br /> Es besteht die Möglichkeit, bei guten Leistungen sich bis zum Senior-Moderatoren Rang hochzuarbeiten.
								</p>
                                <div class="r"><a href="?as=support" class="btn btn-supporter btn-3e"><span>Bewerben</span></a></div>
                                <div class="spacer-e"></div>
                            </div>';
                        }else {
                            echo '<h5 class="text-muted vb head-apply"> Bewerben als Supporter/in</h5>
                            <div class="col-in row">

                                <p class="apply-holder">Die Bewerbungphase ist geschlossen.</p>
                                <div class="spacer-e"></div>
                            </div>';
                        }
                        if($moderatorapply == 1){
                            echo '                            <h5 class="text-muted vb head-apply"> Bewerben als Moderator/in</h5>
                            <div class="col-in row">
                                <p class="apply-holder">Ein Moderator hat ein ganz anderes Aufgabenspektrum als der Supporter. 
								<br />Den größten Teil des Aufgabenspektrums macht das bearbeiten von Reports aus. 
								<br />Ein Moderator bearbeitet Reports im Forum, Teamspeak und auf dem Netzwerk.
<br />								Gegebenenfalls bestraft der Moderator dann die Regelbrecher.
<br /> Auch hier besteht die Möglichkeit sich vom Moderator Rang bis zum Senior-Moderator hochzuarbeiten.
</p>
                                <div class="r"><a href="?as=moderator" class="btn btn-moderator btn-3e"><span>Bewerben</span></a></div>
                                <div class="spacer-e"></div>
                            </div>';
                        }else {
                            echo '<h5 class="text-muted vb head-apply"> Bewerben als Moderator/in</h5>
                            <div class="col-in row">

                                <p class="apply-holder">Die Bewerbungphase ist geschlossen.</p>
                                <div class="spacer-e"></div>
                            </div>';
                        }
                        if($builderapply == 1){
                            echo '                            <h5 class="text-muted vb head-apply"> Bewerben als Builder/in</h5>
                            <div class="col-in row">
							<p class="apply-holder">Am Namen kann man schon heraus lesen, dass der Builder etwas baut.
<br />							Ein Builder baut zum Beispiel Maps für das Beteax.net Netzwerk.
<br /> Daher ist der Builder Rang auch ein sehr Verantwortungsvoller Rang.
<br/> Hierbei besteht die Möglichkeit, bei sehr guten Bauleistungen sich bis zum SrBuilder Rang hochzuarbeiten.
 </p>
                                <div class="r"><a href="?as=builder" class="btn btn-builder btn-3e"><span>Bewerben</span></a></div>
                                <div class="spacer-e"></div>
                            </div>';
                        }else {
                            echo '<h5 class="text-muted vb head-apply"> Bewerben als Builder/in</h5>
                            <div class="col-in row">

                                <p class="apply-holder">Die Bewerbungphase ist geschlossen.</p>
                                <div class="spacer-e"></div>
                            </div>';
                        }
                        if($contentapply == 1){
                            echo '                            <h5 class="text-muted vb head-apply"> Bewerben als Content</h5>
                            <div class="col-in row">
							<p class="apply-holder">Ein Content denkt sich Sachen für das Netzwerk aus.
<br />							Zum Beispiel ein neuen Spielmodus oder auch coole Events.
<br /> Für diesen Rang muss man sehr kreativ sein und etwas gut repräsentieren können. 
</p>
                                <div class="r"><a href="?as=content" class="btn btn-devcontent btn-3e"><span>Bewerben</span></a></div>
                                <div class="spacer-e"></div>
                            </div>';
                        }else {
                            echo '<h5 class="text-muted vb head-apply"> Bewerben als Content</h5>
                            <div class="col-in row">

                                <p class="apply-holder">Die Bewerbungphase ist geschlossen.</p>
                                <div class="spacer-e"></div>
                            </div>';
                        }
                        if($developerapply == 1){
                            echo '<h5 class="text-muted vb head-apply"> Bewerben als Developer/in</h5>
                            <div class="col-in row">
							<p class="apply-holder">Ein Developer hat sehr viel Verantwortung im Team.
							<br />Deshalb sollte man sehr Verantwortungsvoll sein, falls man sich für den Posten als Developer bewirbt.
							<br />Der Developer kümmert sich um die Gameentwicklung oder um die Webentwicklung.
							<br />In der Gameentwicklung entwickelt man zum Beispiel neue Featuers für die Lobby oder neue Minigames.
							<br />In der Webentwickelt entwickelt man neue Featuers für unsere Hompage.</p>
                                <div class="r"><a href="?as=developer" class="btn btn-devcontent btn-3e"><span>Bewerben</span></a></div>
                                <div class="spacer-e"></div>
                            </div>';
                        }else {
                            echo '<h5 class="text-muted vb head-apply"> Bewerben als Developer/in</h5>
                            <div class="col-in row">

                                <p class="apply-holder">Die Bewerbungphase ist geschlossen.</p>
                                <div class="spacer-e"></div>
                            </div>';
                        }
                            ?>


                            <hr />
                    </div>


                </div>
            </div>
        </div>
    </main>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <center>
        <footer class="footer">
            <p style="margin-bottom: 0!important;" class="copyright">&copy; 2018 Beteax Network<br /><a href="https://imprint.beteax.net">Impressum</a></p>
        </footer>
    </center>
    <?php
        if($_GET['apply'] == "send"){
   echo('<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script><script>swal({title: "Endlich fertig",text:"Deine Bewerbung wurde abgesendet!",icon: "success",closeOnEsc: true,
   closeOnClickOutside: true,buttons: false,timer: 2000,});</script>');
        }else if($_GET['apply'] == "fehler"){
   echo('<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script><script>swal({title: "Ooopss?!",text:"Irgendwas ist schiefgelaufen...\nBitte versuche es nochmals, sollte das nicht gehen melde dich auf dem Teamspeak oder im Forum.",icon: "error",closeOnEsc: true,
   closeOnClickOutside: true,buttons: false,timer: 5000,});</script>');
        }else if($_GET['apply'] == "closed"){
   echo('<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script><script>swal({title: "Stop!",text:"Leider ist dieser Rang derzeit in keiner Bewerbungsphase.",icon: "error",closeOnEsc: true,
   closeOnClickOutside: true,buttons: false,timer: 4000,});</script>');
        }?>

</body>

</html>