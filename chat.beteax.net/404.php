<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>404 Error</title>
    <script src="/cdn-cgi/apps/head/-om27AK9-RwSUke0prs8wXvP_ew.js"></script><link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    <link href="https://www.beteax.net/style.css" rel="stylesheet">
</head>
<body>
<div class="content">
    <div class="main">
        <h2>Hoppla!</h2>
        <div class="404-image">
            <img src="https://cdn.beteax.net/webimages/404.jpg" style="padding-right:160px;padding-top:20px;"alt="404-img-alternate-btx"/>
        </div>
        <p><b>Du hast wohl eine Seite betreten die es nicht gibt oder hast versucht auf ein Verzeichnis zuzugreifen was keine Daten für dich enthält.</b></p><div class="etx"></div><p><b>Sollte dieser Fehler noch öfters auftauchen und du dir sicher bist das hier was sein sollte, melde das bitte einem Administrator.</b></p>
    </div>
</div>

</body>
<div class="footer">
        <i class="legal"><a href="https://www.twitter.com/variiuz">Beteax Systems</a> &copy; 2018 All rights reserved. Coded with &hearts; by VariusDev.<br /><i class="fas fa-fighter-jet"></i> <a href="https://imprint.beteax.net#impressum"> Impressum</a>&emsp;&emsp;&emsp;<i class="fas fa-info-circle"></i> <a href="https://imprint.beteax.net#policy">Datenschutzerklärung</a></i>
    </div>
</html>