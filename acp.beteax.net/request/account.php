<?php
include("check.php");
if($_SERVER["REQUEST_METHOD"] == "POST") {
  if($_GET['do'] == "add"){
   $username = $_POST['read1'];
   $password = $_POST['read2'];
   $mail = $_POST['read3'];
   $group = $_POST['read4'];
   $from = $_POST['from'];
   $realgroupid = 1;
   switch ($group) {
     case "Standard":
       $realgroupid = 1;//Standard
       break;
     case "Moderation":
       $realgroupid = 2;//Moderation(Senior)
       break;
     case "Administration":
       $realgroupid = 3; //Administration
       break;
   }
   include("../funcs.php");
   register($username,$password,$mail,$realgroupid,$from);
}else if($_GET['do'] == "remove"){
   $username = $_POST['username'];
   if($username == $_SESSION['username']){
     header("Location: ../accounts&a=own_user");
     exit;
   }
   include("data.php");
   try
   {
     $statement = $pdo->prepare("DELETE FROM adminpanel WHERE user = ?");
     $statement->execute(array(
       $username
     ));
     header('Location: ../accounts?a=removeda1');
   }
   catch(PDOException $e)
   {
     header('Location: ../accounts&a=failsql');
   }
}else if($_GET['do'] == "update"){
   $username = $_POST['username'];
   $mail = $_POST['email'];
   $oldusername = $_POST['user'];
   $password = $_POST['passwort'];
   if($password != "" || $password != null){
     include("../func.php");
     changepw($oldusername,$password);
   }
   include("data.php");
   try
   {
     $statement = $pdo->prepare("UPDATE adminpanel SET mail = ?,user = ? WHERE user = ?");
     $statement->execute(array(
       $mail,
       $username,
       $oldusername
     ));
     header('Location: ../accounts&a=updateda1');
   }
   catch(PDOException $e)
   {
     header('Location: ../accounts&a=failsql');
   }
}else {
  die("No Req");
}
}
?>
