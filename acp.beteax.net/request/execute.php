<?php
include ("check.php");

include ("data.php");

// -----------------EXECUTER CLASS BY VARIUSDEV 2018 USING ATOM.IO-------------------------\\
$todo = $_GET["page"];
$do = $_GET["value"];
function username_to_uuid($username)
{
	$profile = username_to_profile($username);
	if (is_array($profile) and isset($profile['id']))
	{
		return $profile['id'];
	}
	return false;
}
function username_to_profile($username)
{
	if (is_valid_username($username))
	{
		$json = file_get_contents('https://api.mojang.com/users/profiles/minecraft/' . $username);
		if (!empty($json))
		{
			$data = json_decode($json, true);
			if (is_array($data) and !empty($data))
			{
				return $data;
			}
		}
	}
	return false;
}
function uuid_to_username($uuid)
{
	$uuid = minify_uuid($uuid);
	if (is_string($uuid))
	{
		$json = file_get_contents('https://api.mojang.com/user/profiles/' . $uuid . '/names');
		if (!empty($json))
		{
			$data = json_decode($json, true);
			if (!empty($data) and is_array($data))
			{
				$last = array_pop($data);
				if (is_array($last) and isset($last['name']))
				{
					return $last['name'];
				}
			}
		}
	}
	return false;
}
function is_valid_username($string)
{
	return is_string($string) and strlen($string) >= 2 and strlen($string) <= 16 and ctype_alnum(str_replace('_', '', $string));
}
function minify_uuid($uuid)
{
	if (is_string($uuid))
	{
		$minified = str_replace('-', '', $uuid);
		if (strlen($minified) === 32)
		{
			return $minified;
		}
	}
	return false;
}
function format_uuid($uuid)
{
	$uuid = minify_uuid($uuid);
	if (is_string($uuid))
	{
		return substr($uuid, 0, 8) . '-' . substr($uuid, 8, 4) . '-' . substr($uuid, 12, 4) . '-' . substr($uuid, 16, 4) . '-' . substr($uuid, 20, 12);
	}
	return false;
}
function formuntil($string)
{
	$now = time();
	if ($string == "Permanent")
	{
		return "-1";
	}
	else if ($string == "1 Monat")
	{
		return $now + 2629800;
	}
	else if ($string == "2 Monate")
	{
		return $now + 5259600;
	}
	else if ($string == "3 Monate")
	{
		return $now + 7889400;
	}
	else if ($string == "4 Monate")
	{
		return $now + 10519200;
	}
	else if ($string == "5 Monate")
	{
		return $now + 13149000;
	}
	else if ($string == "1 Tag")
	{
		return $now + 86400;
	}
	else if ($string == "10 Tage")
	{
		return $now + 864000;
	}
	else if ($string == "30 Tage")
	{
		return $now + 2592000;
	}
	else if ($string == "1 Jahr")
	{
		return $now + 31557600;
	}
	else if ($string == "2 Jahre")
	{
		return $now + 63115200;
	}
	else
	{
		return "-1";
	}
}
function formban($string)
{
	$now = time() * 1000;
	if ($string == "Permanent")
	{
		return "-1";
	}
	else if ($string == "1 Tag")
	{
		$right = $now + 86400;
		return $right * 1000;
	}
	else if ($string == "10 Tage")
	{
		$right = $now + 864000;
		return $right * 1000;
	}
	else if ($string == "30 Tage")
	{
		$right = $now + 2592000;
		return $right * 1000;
	}
	else if ($string == "60 Tage")
	{
		$right = $now + 5184000;
		return $right * 1000;
	}
	else
	{
		return "-1";
	}
}
if ($todo == 'groups')
{
	$uuid = $_POST['read1'];
	$group = $_POST['read2'];
	$read3 = $_POST['read3'];
	$u = formuntil($read3);
	$statement = $pdo->prepare("UPDATE Groups SET Gruppe = ?, Until = ? WHERE UUID = ?");
	$statement->execute(array(
		$group,
		$u,
		$uuid
	));
	header('Location: ../system?q=groups&a=updatedgroup');
	//
}else if ($todo == 'apply')
{
	$id = $_POST['id'];
	$status = $_POST['state'];
	$statement = $pdo->prepare("UPDATE applys SET CLOSED = ? WHERE id = ?");
	$statement->execute(array(
		$status,
		$id
	));
	header('Location: ../list?q=applys');
	//
}else if ($todo == 'apply')
{
	$id = $_POST['id'];
	$status = $_POST['comment'];
	$statement = $pdo->prepare("UPDATE applys SET KOMMENTAR = ? WHERE id = ?");
	$statement->execute(array(
		$status,
		$id
	));
	header('Location: ../list?q=applys');
	//
}
else if ($todo == 'serverping')
{
	$to = $_POST['read1'];
	$value = $_POST['read2'];
	if ($to == "1 MOTD Linie")
	{
		try
		{
			$statement = $pdo->prepare("UPDATE motd SET zeile1 = ? WHERE id = ?");
			$statement->execute(array(
				$value,
				'1'
			));
		}
		catch(PDOException $e)
		{
			header('Location: ../cloud?q=serverping&a=failsql');
			exit;
		}
	}
	else if ($to == "2 MOTD Linie")
	{
		try
		{
			$statement = $pdo->prepare("UPDATE motd SET zeile2 = ? WHERE id = ?");
			$statement->execute(array(
				$value,
				'1'
			));
		}
		catch(PDOException $e)
		{
			header('Location: ../cloud?q=serverping&a=failsql');
			exit;
		}
	}
	else if ($to == "2 MOTD Linie im Wartungsmodus")
	{
		try
		{
			$statement = $pdo->prepare("UPDATE motd SET wartung = ? WHERE id = ?");
			$statement->execute(array(
				$value,
				'1'
			));
		}
		catch(PDOException $e)
		{
			header('Location: ../cloud?q=serverping&a=failsql');
			exit;
		}
	}
	else if ($to == "Maximale Spieler")
	{
		try
		{
			$statement = $pdo->prepare("UPDATE spieler SET max = ? WHERE id = ?");
			$statement->execute(array(
				$value,
				'1'
			));
		}
		catch(PDOException $e)
		{
			header('Location: ../cloud?q=serverping&a=failsql');
			exit;
		}
	}
	else
	{
		header('Location: ../cloud?q=serverping&a=fail');
		exit;
	}
	header('Location: ../cloud?q=serverping&a=pingupdated');
}
else if ($todo == 'ban')
{
	if ($do == "ban")
	{
		$banner = $_SESSION['username'];
		$e = username_to_uuid($_POST['read1']);
		$banned = format_uuid($e);
		$name = $_POST['read1'];
		$reason = $_POST['read2'];
		$time = $_POST['read3'];
		$realtime = formban($time);
		try
		{
			$statement = $pdo->prepare("INSERT INTO Bans (Von, Name, UUID, Ende, Grund) VALUES (?,?,?,?,?)");
			$statement->execute(array(
				$banner,
				$name,
				$banned,
				$realtime,
				$reason
			));
			header('Location: ../add?q=ban&a=banned');
		}
		catch(PDOException $e)
		{
			header('Location: ../add?q=ban&a=failsql');
		}
	}
	else if ($do == "unban")
	{
		$uuid = $_POST['uuid'];
		try
		{
			$statement = $pdo->prepare("DELETE FROM Bans WHERE UUID = ?");
			$statement->execute(array(
				$uuid
			));
			header('Location: ../list?q=banlist');
		}
		catch(PDOException $e)
		{
			header('Location: ../add?q=banlist&a=failsql');
		}
	}
	else
	{
		header('Location: ../list?q=banlist&a=fail');
	}
}else if ($todo == 'mute')
{
	if ($do == "mute")
	{
		$banner = $_SESSION['username'];
		$e = username_to_uuid($_POST['read1']);
		$banned = format_uuid($e);
		$name = $_POST['read1'];
		$reason = $_POST['read2'];
		$time = $_POST['read3'];
		$realtime = formban($time);
		try
		{
			$statement = $pdo->prepare("INSERT INTO Mutes (Von, Name, UUID, Ende, Grund) VALUES (?,?,?,?,?)");
			$statement->execute(array(
				$banner,
				$name,
				$banned,
				$realtime,
				$reason
			));
			header('Location: ../add?q=mute&a=muted');
		}
		catch(PDOException $e)
		{
			header('Location: ../add?q=mute&a=failsql');
		}
	}
	else if ($do == "unmute")
	{
		$uuid = $_POST['uuid'];
		try
		{
			$statement = $pdo->prepare("DELETE FROM Mutes WHERE UUID = ?");
			$statement->execute(array(
				$uuid
			));
			header('Location: ../list?q=mutelist');
		}
		catch(PDOException $e)
		{
			header('Location: ../add?q=mutelist&a=failsql');
		}
	}
	else
	{
		header('Location: ../list?q=banlist&a=fail');
	}
}
else if ($todo == 'blacklist')
{
	if ($do == "add")
	{
		$adder = $_SESSION['username'];
		$word = $_POST['read1'];
		try
		{
			$statement = $pdo->prepare("INSERT INTO badword (wort, adder) VALUES (?,?)");
			$statement->execute(array(
				$word,
				$adder
			));
			header('Location: ../add?q=blacklist&a=addedbadword');
		}
		catch(PDOException $e)
		{
			header('Location: ../add?q=blacklist&a=failsql');
		}
	}
	else if ($do == "remove")
	{
		$wordid = $_POST['id'];
		try
		{
			$statement = $pdo->prepare("DELETE FROM badword WHERE id = ?");
			$statement->execute(array(
				$wordid
			));
			header('Location: ../list?q=blacklist');
		}
		catch(PDOException $e)
		{
			header('Location: ../list?q=blacklist&a=failsql');
		}
	}
	else
	{
		header('Location: ../list?q=blacklist&a=fail');
	}
}
else if ($todo == 'wartung')
{
	if ($do == "add")
	{
		$username = $_POST['read1'];
		$e = username_to_uuid($username);
		$listed = format_uuid($e);
		try
		{
			$statement = $pdo->prepare("INSERT INTO wartungslist(UUID, name) VALUES (?,?)");
			$statement->execute(array(
				$listed,
				$username
			));
			header('Location: ../system?q=maintenance&a=addedwl');
		}
		catch(PDOException $e)
		{
			header('Location: ../system?q=maintenance&a=failsql');
		}
	}
	else if ($do == "remove")
	{
		$uuid = $_POST['UUID'];
		try
		{
			$statement = $pdo->prepare("DELETE FROM wartungslist WHERE UUID = ?");
			$statement->execute(array(
				$uuid
			));
			header('Location: ../system?q=maintenance&a=removedwl');
		}
		catch(PDOException $e)
		{
			header('Location: ../system?q=maintenance&a=failsql');
		}
	}
	else if ($do == "update")
	{
		$a = $_POST['read1'];
		if ($a == "Wartung aktivieren")
		{
			$b = $_POST['read2'];
			if ($b == '' || $b == null)
			{
				$b = "Automatischer Wartungsmodus";
			}
			try
			{
				$statement = $pdo->prepare("UPDATE proxysettings SET Value='1', reason= ? WHERE id ='1'");
				$statement->execute(array(
					$b
				));
			}
			catch(PDOException $e)
			{
				header('Location: ../system?q=maintenance&a=failsql');
				exit;
			}
		}
		else
		{
			$b = $_POST['read2'];
			try
			{
				$statement = $pdo->prepare("UPDATE proxysettings SET Value='0', reason= ? WHERE id ='1'");
				$statement->execute(array(
					$b
				));
			}
			catch(PDOException $e)
			{
				header('Location: ../system?q=maintenance&a=failsql');
				exit;
			}
		}
		header('Location: ../system?q=maintenance&a=updatedmaintenance');
	}
	else
	{
		header('Location: ../system?q=maintenance&a=fail');
	}
}
else if ($todo == 'ipblock')
{
	if ($do == "add")
	{
		$ip = $_POST['read1'];
		try
		{
			$statement = $pdo->prepare("INSERT INTO iplist (IP) VALUES (?)");
			$statement->execute(array(
				$ip
			));
			header('Location: ../system?q=ip&a=addedip');
		}
		catch(PDOException $e)
		{
			header('Location: ../system?q=ip&a=failsql');
		}
	}
	else if ($do == "remove")
	{
		$ip = $_POST['IP'];
		try
		{
			$statement = $pdo->prepare("DELETE FROM iplist WHERE IP=?");
			$statement->execute(array(
				$ip
			));
			header('Location: ../system?q=ip');
		}
		catch(PDOException $e)
		{
			header('Location: ../system?q=ip&a=failsql');
		}
	}
	else
	{
		header('Location: ../system?q=ip&a=fail');
	}
}
else
{
	die("ripperino :(");
}
?>
