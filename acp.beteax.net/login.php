<?php
include("request/data.php");
session_start();
if($_SESSION["access_allowed"] == 1){
    header('Location: dashboard');
}  
//Ask if POST Method is there
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      //Escape the String for SQL
      $username = $_POST['username'];
      $bfstring = $_POST['userpass'];
	  //Including new Cryptic Functions
	  include("funcs.php");
	  //Hash the Password from Formular to our cryp method
	  $mdpassword = hashpw($bfstring);
	  
	  /*
	  Now we're execute SQL with new SQL PDO Methods
	  */
      $statement = $pdo->prepare("SELECT * FROM adminpanel WHERE user = ? and pass = ?");
      $statement->execute(array($username,$mdpassword)); 
      $count = $statement->rowCount();
    echo ".";
      if($count == 1) {
          $inj = "SELECT * FROM adminpanel WHERE user='$username'";
          foreach ($pdo->query($inj) as $row) { $groupid = $row['groupid'];}
         $_SESSION['access_allowed'] = 1;
		 $_SESSION['groupid'] = $groupid;
         $_SESSION['username'] = $username;
         header("Location: dashboard");
      }else if($count == 0){
		  header("Location: login?v=DrGHe");
      }
   }
?>
    <!DOCTYPE html>
    <html lang="de">

    <head>
        <title>Beteax Webinterface × Anmelden</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/solid.css" integrity="sha384-Rw5qeepMFvJVEZdSo1nDQD5B6wX0m7c5Z/pLNvjkB14W6Yki1hKbSEQaX9ffUbWe" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/regular.css" integrity="sha384-EWu6DiBz01XlR6XGsVuabDMbDN6RT8cwNoY+3tIH+6pUCfaNldJYJQfQlbEIWLyA" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/brands.css" integrity="sha384-VGCZwiSnlHXYDojsRqeMn3IVvdzTx5JEuHgqZ3bYLCLUBV8rvihHApoA1Aso2TZA" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/fontawesome.css" integrity="sha384-GVa9GOgVQgOk+TNYXu7S/InPTfSDTtBalSgkgqQ7sCik56N9ztlkoTr2f/T44oKV" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/fonts/iconic/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="css/loginsources/vendor/animate/animate.css">
        <link rel="stylesheet" type="text/css" href="css/loginsources/vendor/css-hamburgers/hamburgers.min.css">
        <link rel="stylesheet" type="text/css" href="css/loginsources/vendor/animsition/css/animsition.min.css">
        <link rel="stylesheet" type="text/css" href="css/loginsources/vendor/select2/select2.min.css">
        <link rel="stylesheet" type="text/css" href="css/loginsources/vendor/daterangepicker/daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="css/loginsources/util.css">
        <link rel="stylesheet" type="text/css" href="css/loginsources/main.css">
    </head>

    <body>

        <div class="limiter">
            <div class="container-login100" style="background-image: url('css/loginsources/img/beteaxap.jpg');">
                <div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-54">
                    <form class="login100-form validate-form" action="" method="post">
                        <span class="login100-form-title p-b-49">
						Anmelden
					</span>

                        <div class="wrap-input100 validate-input m-b-23" data-validate="Du must einen Benutzernamen angeben">
                            <span class="label-input100">Benutzername</span>
                            <input class="input100" type="text" name="username" placeholder="Benutzername">
                            <span class="focus-input100" data-symbol="&#xf206;"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate="Password is required">
                            <span class="label-input100">Passwort</span>
                            <input class="input100" type="password" name="userpass" placeholder="Passwort">
                            <span class="focus-input100" data-symbol="&#xf190;"></span>
                        </div>

                        <div class="text-right p-t-8 p-b-31">
                            <a href="recover">
							Passwort vergessen?
						</a>
                        </div>

                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
								Einloggen
							</button>
                            </div>
                        </div>

                        <div class="flex-c-m">
                            <a href="ts3server://ts.beteax.net" class="login100-social-item bg1">
							<i class="fab fa-teamspeak"></i>
						</a>

                            <a href="https://www.twitter.com/beteaxnetwork" class="login100-social-item bg2">
							<i class="fab fa-twitter"></i>
						</a>

                            <a href="https://www.youtuber.com/" class="login100-social-item bg3">
							<i class="fab fa-youtube"></i>
						</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <div id="dropDownSelect1"></div>
        <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
        <script src="vendor/animsition/js/animsition.min.js"></script>
        <script src="vendor/bootstrap/js/popper.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendor/select2/select2.min.js"></script>
        <script src="vendor/daterangepicker/moment.min.js"></script>
        <script src="vendor/daterangepicker/daterangepicker.js"></script>
        <script src="vendor/countdowntime/countdowntime.js"></script>
        <script src="js/main.js"></script>
        <?php
        if($_GET['v'] == "DrGHe"){
   echo('<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script><script>swal("Fehler", "Du hast ein Falsches Passwort oder einen falschen Benutzernamen angegeben.", "error");</script>');
        } ?>

    </body>

    </html>
