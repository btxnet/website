<?php
include("plugins/check.php");
$username = $_SESSION['username'];
include("plugins/data.php");
include("request/adminaccess.php");
$sql = $db;
?>
<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Beteax Webinterface">
    <meta name="author" content="Hibiikiii & Variiuz">
    <title>Beteax Webinterface × Angemeldet als <?php echo($username);?></title>
    <link href="css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/lib/calendar2/semantic.ui.min.css" rel="stylesheet">
    <link href="css/lib/calendar2/pignose.calendar.min.css" rel="stylesheet">
    <link href="css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="css/helper.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>

<body class="fix-header fix-sidebar">

    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
			<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <div id="main-wrapper">
	<?php
	  include("templates/globalvars/head.php");
	?>
        <div class="left-sidebar">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li><a href="dashboard"><i class="fas fa-home"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li class="nav-label">System</li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-ban"></i><span class="hide-menu">Ban</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="add?q=ban"><i class="fas fa-plus"></i>   Ban hinzufügen</a></li>
                                                                        <?php
                                                                        if($_SESSION['groupid'] != 1){
                                                                          echo '<li><a href="list?q=banlist"><i class="fas fa-th-list"></i>   Banliste einsehen</a></li>';
                                                                        }
                                                                         ?>
                                                                         <li><a href="list?q=mybanlist"><i class="fas fa-th-list"></i>  Meine Banliste einsehen</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-bullhorn"></i><span class="hide-menu">Mute</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="add?q=mute"><i class="fas fa-plus"></i>   Mute hinzufügen</a></li>
                                                                        <?php
                                                                        if($_SESSION['groupid'] != 1){
                                                                          echo '<li><a href="list?q=mutelist"><i class="fas fa-th-list"></i>   Muteliste einsehen</a></li>';
                                                                        }
                                                                         ?>
                                                                         <li><a href="list?q=mymutelist"><i class="fas fa-th-list"></i>  Meine Muteliste einsehen</a></li>
                            </ul>
                        </li>
                         <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-users"></i><span class="hide-menu">Gruppensystem</span></a>
                            <ul aria-expanded="false" class="collapse">
<li><a href="system?q=groups"><i class="fas fa-users-cog"></i>   Spielergruppen ändern</a></li>
                                        <li><a href="list?q=groups"><i class="fas fa-th-list"></i>   Spielergruppen einsehen</a></li>
                            </ul>
                        </li>
                        <li  class="active"> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-archive"></i><span class="hide-menu">Sonstiges</span></a>
                            <ul aria-expanded="true" class="collapse">
                                <li><a href="add?q=blacklist"><i class="fas fa-plus"></i>  Wort hinzufügen</a></li>
<li  class="active"><a href="list?q=blacklist"  class="active"><i class="fas fa-th-large"></i>   Blacklist einsehen</a></li>
<li><a href="list?q=applys"><i class="fas fa-th-large"></i>   Bewerbungen einsehen</a></li>
                                        <li><a href="system?q=ip"><i class="fas fas fa-list-ol"></i>   IPBlock's einsehen</a></li>
                            </ul>
                        </li>
                        <li class="nav-label">CloudSystem</li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-server"></i><span class="hide-menu">Server</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-cloud"></i><span class="hide-menu"> Cloud</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="cloud?q=controller"><i class="fas fa-cloud"></i> Cloudcontrol</a></li>
                                    </ul>
                                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fab fa-connectdevelop"></i><span class="hide-menu"> Proxys</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="ui-button.html"><i class="fab fa-connectdevelop"></i> Proxy-1</a></li>
                                    </ul>
                                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-desktop"></i><span class="hide-menu"> Lobbys</span></a>
                                <ul aria-expanded="false" class="collapse">
                                    <li><a href="ui-button.html"><i class="fas fa-desktop"></i> Lobby-1</a></li>
                                    </ul>
                                <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-gamepad"></i><span class="hide-menu"> GameServer</span></a>
                                <ul aria-expanded="false" class="collapse">
                                <li><a href="ui-button.html"><i class="fas fa-bed"></i> BW-01</a></li>
                                     <li><a href="ui-button.html"><i class="fas fa-gift"></i> PartyTime-01</a></li>
                                    <li><a href="ui-button.html"><i class="fas fa-tshirt"></i> QuickSG-01</a></li>
                                    <li><a href="ui-button.html"><i class="fas fa-clock"></i> TimeArchieve-01</a></li>

                                    </ul>

                            </ul>
                        </li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-sign"></i><span class="hide-menu">Schildersystem</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="add?q=template"><i class="fas fa-plus"></i> Template erstellen</a></li>
                                <li><a href="list?q=template"><i class="fas fa-th-list"></i> Templateliste einsehen</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow " href="#" aria-expanded="false"><i class="fas fa-cog"></i><span class="hide-menu">Proxy-Einstellungen</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="cloud?q=serverping"><i class="fas fa-pencil-alt"></i> MOTD ändern</a></li>
                                <li><a href="system?q=maintenance"><i class="fas fa-th-list"></i> Wartungseinstellungen</a></li>
                            </ul>
                        </li>
                        <li class="nav-label">Management</li>
                        <!--<li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fas fa-envelope"></i><span class="hide-menu">Mailserver</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="email-compose.html"><i class="fas fa-plus"></i> Email schreiben</a></li>
                                        <li><a href="page-register.html"><i class="fas fa-inbox"></i> Email Inbox</a></li>
                            </ul>
                        </li>-->
                        <li><a href="https://beteax.net/phpmyadmin"><i class="fas fa-database"></i><span class="hide-menu"> MySQL Datenbank</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">System</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">System</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Sonstiges</a></li>
                        <li class="breadcrumb-item active">Blacklist einsehen</li>
                    </ol>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Blacklist</h4>
                                <h6 class="card-subtitle">Hier siehst du alle Wörter die vom System im Minecraft-Chat deaktiviert werden.</h6>
                                <div class="table-responsive m-t-40">
                                    <table id="myTable" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th style="width:50%;">Wort</th>
                                                <th style="width:40%;">Hinzugefügt von</th>
                                                <th style="width:10%;">Aktion</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php
                                           $inj = "SELECT * FROM badword ORDER BY wort ASC";
                                           foreach ($pdo->query($inj) as $row) {
                                        echo "<tr>";
                                        echo "<td>" . $row['wort'] . "</td>";
                                        echo "<td>" . $row['adder'] . "</td>";
                                        echo '<td><form action="request/execute.php?page=blacklist&value=remove" method="post"><input type="hidden" name="id" value='.$row['id'].'><button type="submit" class="btn btn-danger btn-flat btn-addon m-b-10 m-l-5"><i class="fas fa-trash"></i></button> </form></td>';
                                        echo "</tr>"; } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            			<?php include("templates/globalvars/footer.php"); ?>
        </div>
    </div>
    <script src="js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="js/lib/bootstrap/js/popper.min.js"></script>
    <script src="js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="js/scripts.js"></script>


    <script src="js/lib/datatables/datatables.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="js/lib/datatables/datatables-init.js"></script>
			<?php include("templates/globalvars/alerts.php"); ?>
</body>

</html>
