<!-- Head templater START -->
 <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header">
                    <a class="navbar-brand" href="dashboard">
                        <b><img src="images/logo.png" alt="homepage" class="dark-logo" /></b>
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0">

                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!--<li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Suchen"> <a class="srh-btn"><i class="ti-close"></i></a> </form>
                        </li>-->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i>
								<div class="notify"> <!--<span class="heartbit"></span> <span class="point"></span>--> </div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn">
                                <ul>
                                    <li>
                                        <div class="drop-title">Benachrichtigungen</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                           <!-- <a href="#">
                                                <div class="btn btn-danger btn-circle m-r-10"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>This is title</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span>
                                                </div>
                                            </a>-->
											                                <?php
                                    $inj = "SELECT * FROM adminpanel_news ORDER BY id DESC LIMIT 4";
                                    foreach ($pdo->query($inj) as $row) {
                                        echo '<div class="user-img"> <img src="https://minotar.net/helm/'.$row['who'].'.png" width="36" alt="user" class="img-circle"><span class="profile-status online pull-right"></span>';
                                        echo '</div>';
                                        echo '<div class="mail-contnet">';
                                        echo '<h5>'.$row['what'].'</h5> <span class="mail-desc">'.$row['message'].'</span> <span class="time">'.$row['who'].'</span>';
                                        echo '</div>';
                                        echo '<br />';
                                    }
                                    mysqli_close($con);
                                    ?>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted " href="#" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-envelope"></i>
								<div class="notify"> <!--<span class="heartbit"></span> <span class="point"></span>--> </div>
							</a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" aria-labelledby="2">
                                <ul>
                                    <li>
                                        <div class="drop-title">Intramails</div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                           <!-- <a href="#">
                                                <div class="user-img"> <img src="images/users/5.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Michael Qin</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span>
                                                </div>
                                            </a>-->
                                            <a href="">
                                                <div class="btn btn-danger btn-circle m-r-10"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>System</h5> <span class="mail-desc">Du hast keine neuen Emails!</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Alle Emails ansehen</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo('<img src="https://minotar.net/helm/'.$username.'.png" alt="userpic" width="36" class="img-circle">'); ?></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
								<?php if($_SESSION['groupid'] == 3){
									echo '                                    <li><a href="accounts"><i class="ti-user"></i> Accounts verwalten</a>';
								}?>
                                    <li><a href="profile"><i class="ti-settings"></i> Einstellungen</a></li>
                                    <li><a href="logout"><i class="fa fa-power-off"></i> Abmelden</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
		<!-- Head templater STOP -->
