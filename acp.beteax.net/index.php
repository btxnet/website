<?php
session_start();
if($_SESSION["access_allowed"] != 1){
	$_SESSION["access_allowed"] = 0;
	header('Location: login');
}else {
    header('Location: dashboard');
}
?>
