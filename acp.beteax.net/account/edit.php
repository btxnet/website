                                    <form class="form-valide" action="request/account.php?do=edit" method="post" autocomplete="off">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-motd">Username <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" name="read1" class="form-control" placeholder="Benutzername" autocomplete="off"  required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-motd">Passwort <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="password" name="read2" class="form-control" placeholder="Benutzerpasswort" autocomplete="off" required>
                                            </div>
                                        </div>
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-motd">Mail</label>
                                            <div class="col-lg-6">
                                                <input type="email" name="read3" class="form-control" placeholder="Benutzermail" autocomplete="off" value="master@beteaxmail.de">
                                            </div>
                                        </div>
										<div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-motd">Gruppe <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select name="read4" class="form-control" id="val-option">
										<option value="" selected disabled hidden>Bitte Auswählen</option>
                                                    <option>Standard</option>
                                                    <option>Moderation</option>
                                                    <option>Administration</option>
                                                    <option disabled>Super Administration</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" align="center" class="btn btn-primary">Hinzufügen</button>
                                            </div>
                                        </div>
                                    </form>